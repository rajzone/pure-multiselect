import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import './main.css';

import withMultiSelectState from "./multi_select_state";
import SourceList from "./source_list";
import DestinationList from "./destination_list";

export class MultiSelect extends PureComponent {
  static propTypes = {
    selectedItems: PropTypes.array,
    filteredItems: PropTypes.array,
    loading: PropTypes.bool,
    messages: PropTypes.object,
    onChange: PropTypes.func,
    showSearch: PropTypes.bool,
    showSelectAll: PropTypes.bool,
    showSelectedItems: PropTypes.bool,
    searchIcon: PropTypes.string,
    deleteIcon: PropTypes.string,
    searchRenderer: PropTypes.func,
    selectedItemRenderer: PropTypes.any,
    height: PropTypes.number,
    itemHeight: PropTypes.number,
    selectAllHeight: PropTypes.number,
    maxSelectedItems: PropTypes.number
  };

  static defaultProps = {
    selectedItems: [],
    filteredItems: [],
    messages: {},
    showSearch: true,
    showSelectAll: true,
    showSelectedItems: true,
    height: 329,
    itemHeight: 35,
  };

  calculateHeight() {
    let {
      height,
      showSearch,
      showSelectAll,
      itemHeight,
      selectAllHeight,
      maxSelectedItems
    } = this.props;
    height = showSearch && !maxSelectedItems ? height - 45 : height;
    height = showSelectAll
      ? height - (selectAllHeight ? selectAllHeight : itemHeight)
      : height;
    return height;
  }

  render() {
    const {
      showSearch,
      height,
      itemHeight,
      selectAllHeight,
      showSelectAll,
      showSelectedItems,
      itemRenderer,
      selectedItemRenderer,
      filteredItems,
      selectedItems,
      getList,
      filterItems,
      isAllSelected,
      clearAll,
      selectItem,
      unselectItems,
      searchIcon,
      searchRenderer,
      selectionStatusRenderer,
      noItemsRenderer,
      messages,
      loading,
      maxSelectedItems
    } = this.props;
    const calculatedHeight = this.calculateHeight();
    const selectedIds = selectedItems.map(item => item.id);
    const disabled =
      maxSelectedItems && maxSelectedItems <= selectedItems.length;
    return (
      <div
        className="containerdiv"
        style={{ height }}
      >
        <div>
        {!loading && (
          <SourceList
            searchRenderer={searchRenderer}
            showSearch={showSearch}
            filterItems={filterItems}
            searchIcon={searchIcon}
            messages={messages}
            showSelectAll={showSelectAll && !maxSelectedItems}
            itemHeight={itemHeight}
            isAllSelected={isAllSelected}
            selectedIds={selectedIds}
            itemRenderer={itemRenderer}
            getList={getList}
            filteredItems={filteredItems}
            calculatedHeight={calculatedHeight}
            selectItem={selectItem}
            noItemsRenderer={noItemsRenderer}
            disabled={disabled}
            selectAllHeight={selectAllHeight}
          />
        )}
        {!loading &&
          showSelectedItems && (
            <DestinationList
              selectionStatusRenderer={selectionStatusRenderer}
              selectedIds={selectedIds}
              clearAll={clearAll}
              messages={messages}
              selectedItems={selectedItems}
              itemHeight={itemHeight}
              height={height}
              unselectItems={unselectItems}
              selectedItemRenderer={selectedItemRenderer}
              noItemsRenderer={noItemsRenderer}
            />
          )}

        </div>

      </div>
    );
  }
}

export default withMultiSelectState(MultiSelect);
