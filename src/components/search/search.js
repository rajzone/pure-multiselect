import React from "react";
import PropTypes from "prop-types";

const SearchIcon = 'x';

const Search = ({ searchPlaceholder, searchIcon, onChange }) => {
  const IconRenderer = searchIcon;
  return (
    <div>
      <input
        type="text"
        placeholder={searchPlaceholder}
        onChange={onChange}
      />
      <div>
        <IconRenderer />
      </div>
    </div>
  );
};

Search.propTypes = {
  searchPlaceholder: PropTypes.string,
  searchIcon: PropTypes.any,
  onChange: PropTypes.func
};

Search.defaultProps = {
  searchPlaceholder: "Search...",
  searchIcon: SearchIcon
};

export default Search;
