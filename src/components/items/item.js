import React from "react";
import PropTypes from "prop-types";
//import classnames from "classnames";
import ItemLabel from "./item_label";

const Item = ({
  item,
  height,
  onClick,
  withBorder,
  checked,
  indeterminate,
  disabled
}) => (
  <div
    className="listContainer"
    style={{ height }}
    onClick={onClick}
  >
    <input
          type="checkbox"
          color="primary"
          checked={checked}
          indeterminate={indeterminate}
          disabled={disabled}
          />

    <ItemLabel label={item.name} />
  </div>
);

Item.propTypes = {
  item: PropTypes.object,
  height: PropTypes.number,
  withBorder: PropTypes.bool,
  checked: PropTypes.bool,
  indeterminate: PropTypes.bool,
  disabled: PropTypes.bool
};

Item.defaultProps = {
  item: {},
  height: 40,
  withBorder: false,
  checked: false,
  indeterminate: false,
  disabled: false
};

export default Item;
