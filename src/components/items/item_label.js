import React from "react";
import PropTypes from "prop-types";
import ReactOverflowTooltip from "react-overflow-tooltip";

const ItemLabel = ({ label }) => (
  <ReactOverflowTooltip title={label}>
    <div >{label}</div>
  </ReactOverflowTooltip>
);

ItemLabel.propTypes = {
  label: PropTypes.string
};

ItemLabel.defaultProps = {
  label: ""
};

export default ItemLabel;
