import React from "react";
import PropTypes from "prop-types";


const NoItems = ({ noItemsMessage }) => (
  <div >{noItemsMessage}</div>
);

NoItems.propTypes = {
  noItemsMessage: PropTypes.string
};

NoItems.defaultProps = {
  noItemsMessage: ""
};

export default NoItems;
