import React from "react";
import PropTypes from "prop-types";
import ItemLabel from "./item_label";

const SelectedItem = ({ item, height }) => (
  <div className="listContainerR">
    <ItemLabel label={item.name} />
    <div className="delete">x</div>
  </div>
);

SelectedItem.propTypes = {
  item: PropTypes.object,
  height: PropTypes.number
};

SelectedItem.defaultProps = {
  item: {},
  height: 40
};

export default SelectedItem;
