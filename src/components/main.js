import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "body": {
        "backgroundColor": "white",
        "fontFamily": "Arial, Helvetica, sans-serif"
    },
    "containerdiv": {
        "width": "100%",
        "marginLeft": "15%",
        "marginTop": "2%",
        "marginRight": "15%",
        "borderLeftStyle": "solid",
        "borderLeftWidth": "thin",
        "borderLeftColor": "#1A9ECE",
        "float": "left",
        "height": "100%"
    },
    "footerdiv": {
        "clear": "both",
        "width": "100%",
        "height": "2%",
        "float": "left",
        "marginTop": "43%",
        "backgroundColor": "#EDEDEE"
    },
    "footLeft": {
        "paddingLeft": "2%",
        "marginTop": "2.5%",
        "marginRight": "2.5%",
        "marginBottom": "2.5%",
        "marginLeft": "2.5%"
    },
    "rightPane": {
        "width": "99%"
    },
    "clearAll": {
        "float": "right"
    },
    "footer": {
        "height": "2%"
    },
    "footRight": {
        "float": "right",
        "marginTop": "52.5%",
        "width": "100%",
        "whiteSpace": "nowrap"
    },
    "a:link": {
        "color": "#57B8D9",
        "backgroundColor": "transparent",
        "textDecoration": "none"
    },
    "a:visited": {
        "color": "#57B8D9"
    },
    "input[type=text]": {
        "backgroundColor": "#F3F3F3",
        "height": 35,
        "width": "calc(100% - 22px)",
        "paddingLeft": 20,
        "marginBottom": 5,
        "fontSize": 16,
        "border": "none",
        "borderBottom": "3px solid #ADDCED",
        "overflow": "hidden"
    },
    "itemHeader": {
        "paddingTop": "3%",
        "paddingRight": "3%",
        "paddingBottom": "3%",
        "paddingLeft": "3%",
        "borderRightColor": "#ADDCED",
        "backgroundColor": "#1A9ECE",
        "color": "white"
    },
    "listContainer": {
        "width": "100%",
        "paddingLeft": "2%",
        "whiteSpace": "nowrap",
        "overflowY": "scroll",
        "overflowX": "hidden",
        "outline": "none",
        "verticalAlign": "middle"
    },
    "listContainer > div": {
        "height": 35,
        "display": "inline-block"
    },
    "listContainer:hover": {
        "backgroundColor": "#E4F9FE"
    },
    "delete": {
        "border": "none",
        "color": "#DCDCDC",
        "fontSize": 23,
        "float": "right",
        "paddingRight": 10
    },
    "listContainerR": {
        "width": "100%",
        "paddingLeft": "1%",
        "whiteSpace": "nowrap",
        "overflowY": "scroll",
        "overflowX": "hidden"
    },
    "listContainerR > div": {
        "display": "inline-block"
    },
    "listContainerR:hover": {
        "backgroundColor": "#E4F9FE"
    },
    "button": {
        "backgroundColor": "#00B6E8",
        "textDecorationColor": "#ADDCED",
        "border": "none",
        "color": "white",
        "paddingTop": 10,
        "paddingRight": 16,
        "paddingBottom": 10,
        "paddingLeft": 16,
        "textAlign": "center",
        "textDecoration": "none",
        "fontSize": 12,
        "borderRadius": 4
    },
    "column": {
        "width": "39%",
        "borderRight": "1px solid #F3F3F3",
        "marginRight": 0,
        "float": "left"
    },
    "checkBoxStyle": {
        "verticalAlign": "middle"
    },
    "checkList": {
        "verticalAlign": "middle"
    },
    "checkList:checked": {
        "backgroundColor": "grey"
    },
    "input[type=checkbox]:checked ~ div": {
        "backgroundColor": "#1A9ECE",
        "width": "99%",
        "color": "white"
    }
});