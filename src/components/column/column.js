import React from "react";

const Column = ({ children }) => (
  <div className="column">{children}</div>
);

export default Column;
