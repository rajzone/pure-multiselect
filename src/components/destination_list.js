import React from "react";
import PropTypes from "prop-types";

import Column from "./column/column";
import List from "./list/items_list";
import SelectedItem from "./items/selected_item";
import SelectionStatus from "./selection_status/selection_status";


const divStyle = {
  paddingTop: '5%',
  paddingLeft: '75%',
  backgroundColor: '#EDEDEE',
  color: '#57B8D9',
  height: '34px',
};

const DestinationList = ({
  selectionStatusRenderer,
  selectedIds,
  clearAll,
  messages,
  selectedItems,
  itemHeight,
  height,
  unselectItems,
  selectedItemRenderer,
  clearAllMessage
}) => {
  const SelectionStatusRenderer = selectionStatusRenderer;
  return (
    <div>
    <Column>

      <SelectionStatusRenderer
        selected={selectedIds}
        clearAll={clearAll}
        clearAllMessage={messages.clearAllMessage}
        selectedMessage={messages.selectedMessage}
        noneSelectedMessage={messages.noneSelectedMessage}
      />
      <List
        items={selectedItems}
        itemHeight={itemHeight}
        height={height - 110}
        onClick={(event, id) => unselectItems([id])}
        renderer={selectedItemRenderer}
      />
      <div class="footer">
        <div class="footRight">
          <div style={divStyle} onClick={clearAll}>
            {clearAllMessage}
          </div>
        </div>
      </div>
    </Column>

    </div>
  );
};

DestinationList.propTypes = {
  selectionStatusRenderer: PropTypes.any,
  selectedIds: PropTypes.arrayOf(PropTypes.number),
  clearAll: PropTypes.func,
  messages: PropTypes.object,
  selectedItems: PropTypes.array,
  itemHeight: PropTypes.number,
  height: PropTypes.number,
  unselectItems: PropTypes.func,
  selectedItemRenderer: PropTypes.any,
  clearAllMessage: PropTypes.string,
};

DestinationList.defaultProps = {
  selectionStatusRenderer: SelectionStatus,
  selectedIds: [],
  messages: {},
  selectedItems: [],
  itemHeight: 35,
  height: 229,
  selectedItemRenderer: SelectedItem,
  clearAllMessage: "Clear Selection",
};

export default DestinationList;
