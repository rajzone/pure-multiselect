import React from "react";
import PropTypes from "prop-types";

const SelectionStatus = ({
  selected,
  clearAll,
  clearAllMessage,
  noneSelectedMessage,
  selectedMessage
}) => (
  <div>
    <div className="itemHeader">
      {selected.length > 0
        ? `${selectedMessage} ( ${selected.length} )`
        : noneSelectedMessage}
    </div>

  </div>
);

SelectionStatus.propTypes = {
  selected: PropTypes.array,
  clearAll: PropTypes.func,
  clearAllMessage: PropTypes.string,
  noneSelectedMessage: PropTypes.string,
  selectedMessage: PropTypes.string
};

SelectionStatus.defaultProps = {
  selected: [],
  clearAllMessage: "Clear Selection",
  noneSelectedMessage: "Selected Items ( 0 )",
  selectedMessage: "Selected Items"
};

export default SelectionStatus;
