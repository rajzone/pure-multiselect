import React from "react";
import PropTypes from "prop-types";

import Column from "./column/column";
import List from "./list/items_list";
import Search from "./search/search";
import Item from "./items/item";

const bStyle = {
  marginRight: '5%',
};

const SourceList = ({
  searchRenderer,
  showSearch,
  filterItems,
  searchIcon,
  messages,
  itemHeight,
  selectAllHeight,
  selectAllItems,
  isAllSelected,
  selectedIds,
  itemRenderer,
  getList,
  filteredItems,
  calculatedHeight,
  selectItem,
  disabled
}) => {
  const SearchRenderer = searchRenderer;
  return (
    <Column>
      <div className="itemHeader">
        Unselected Items
      </div>
      {showSearch && (
        <SearchRenderer
          onChange={filterItems}
          searchIcon={searchIcon}
          searchPlaceholder={messages.searchPlaceholder}
        />
      )}
      <List
        ref={getList}
        offset={1}
        items={filteredItems}
        itemHeight={itemHeight}
        height={calculatedHeight}
        onClick={selectItem}
        selectedIds={selectedIds}
        renderer={itemRenderer}
        disabled={disabled}
        disabledItemsTooltip={messages.disabledItemsTooltip}
      />

      <div className="footerdiv">
        <div className="footLeft">
          <button style={bStyle}>Done </button>
          <a href="#" >Cancel </a>
        </div>
      </div>
    </Column>
  );
};

SourceList.propTypes = {
  searchRenderer: PropTypes.any,
  itemRenderer: PropTypes.any,
  searchIcon: PropTypes.any,
  showSearch: PropTypes.bool,
  isAllSelected: PropTypes.bool,
  filterItems: PropTypes.func,
  messages: PropTypes.object,
  itemHeight: PropTypes.number,
  selectAllHeight: PropTypes.number,
  calculatedHeight: PropTypes.number,
  filteredItems: PropTypes.array,
  selectedIds: PropTypes.arrayOf(PropTypes.number),
  selectAllItems: PropTypes.func,
  getList: PropTypes.func,
  selectItem: PropTypes.func,
  disabled: PropTypes.bool
};

SourceList.defaultProps = {
  searchRenderer: Search,
  itemRenderer: Item,
  showSearch: true,
  isAllSelected: false,
  calculatedHeight: 329,
  itemHeight: 35,
  selectedIds: [],
  filteredItems: [],
  messages: {},
  disabled: false
};

export default SourceList;
