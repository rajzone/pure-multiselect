import React, { Component } from 'react';
import PureMultiSelect from './components/multi_select';

class App extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      items: [
        { id: 1, name: "item1" },
        { id: 2, name: "item2" },
        { id: 3, name: "item3" },
        { id: 4, name: "item4" },
        { id: 5, name: "item5" }
      ],
      selectedItems: []
    };
  }

  handleChange(selectedItems) {
    this.setState({ selectedItems });
  }

  render() {
    const { items, selectedItems } = this.state;
    return (
        <PureMultiSelect
        items={items}
        selectedItems={selectedItems}
        onChange={this.handleChange}
      />

    );
  }
}

export default App;
