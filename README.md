## Multi-select 

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Installation

Run the below command in terminal

### `npm install`

This will install all the dependencies<br>

## Start

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.<br>

#### Tested in macOs High Sierra V10.13.5

<img src="https://gitlab.com/rajzone/pure-multiselect/raw/master/primarySnapShot.png" />

<img src="https://gitlab.com/rajzone/pure-multiselect/raw/master/selectionSnapShot.png" />

<img src="https://gitlab.com/rajzone/pure-multiselect/raw/master/searchSnapShot.png" />